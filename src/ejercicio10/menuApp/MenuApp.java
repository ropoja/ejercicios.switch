package ejercicio10.menuApp;

import ejercicio10.geometria.CalculadoraAreas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuApp {

    public static void main(String[] args) {
        CalculadoraAreas calculadoraAreas = CalculadoraAreas.getInstancia();
        int opc = 0;
        double aux1, aux2;

        System.out.println("** Calculadora de Áreas **\n");

        try (Scanner scanner = new Scanner(System.in)) {
            do {
                System.out.println("¿Qué área necesita calcular?");
                System.out.println("1. Triángulo");
                System.out.println("2. Cuadrado");
                System.out.println("3. Círculo");
                System.out.println("4. Rectángulo");
                System.out.println("5. Salir\n");

                System.out.print("> ");

                try {
                    switch (opc = scanner.nextInt()) {
                        case 1:
                            System.out.print("Base: ");
                            aux1 = scanner.nextDouble();

                            if (!esNumeroValido(aux1)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.print("Altura: ");
                            aux2 = scanner.nextDouble();

                            if (!esNumeroValido(aux2)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.println("El área de su triángulo es: " + calculadoraAreas.calcularAreaTriangulo(aux1, aux2));
                            break;
                        case 2:
                            System.out.print("Lado: ");
                            aux1 = scanner.nextDouble();

                            if (!esNumeroValido(aux1)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.println("El área de su cuadrado es: " + calculadoraAreas.calcularAreaCuadrado(aux1));
                            break;
                        case 3:
                            System.out.print("Radio: ");
                            aux1 = scanner.nextDouble();

                            if (!esNumeroValido(aux1)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.println("El área de su círculo es: " + calculadoraAreas.calcularAreaCirculo(aux1));
                            break;
                        case 4:
                            System.out.print("Base: ");
                            aux1 = scanner.nextDouble();

                            if (!esNumeroValido(aux1)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.print("Altura: ");
                            aux2 = scanner.nextDouble();

                            if (!esNumeroValido(aux2)) throw new IllegalArgumentException("El número debe ser mayor a 0.");

                            System.out.println("El área de su rectángulo es: " + calculadoraAreas.calcularAreaRectangulo(aux1, aux2));
                            break;
                        case 5:
                            System.out.println("Saliendo...");
                            break;
                        default:
                            System.out.println("Opción inválida.");
                    }
                } catch (IllegalArgumentException ex) {
                    System.out.println("Número inválido. " + ex.getMessage());
                } catch (InputMismatchException ex) { // En caso de captura de caracteres no numéricos.
                    System.out.println("Opción inválida.");
                    scanner.next();
                }

                System.out.println("");
            } while (opc != 5);
        }
    }

    //region Validaciones
    private static boolean esNumeroValido(double numero) {
        return numero > 0.0;
    }
    //endregion
}
