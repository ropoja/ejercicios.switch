package ejercicio10.geometria;

public class CalculadoraAreas {
    //region Singleton

    private static CalculadoraAreas INSTANCIA_UNICA = null;

    public static CalculadoraAreas getInstancia() {
        if (INSTANCIA_UNICA == null) {
            INSTANCIA_UNICA = new CalculadoraAreas();
        }

        return INSTANCIA_UNICA;
    }

    private CalculadoraAreas() {
    }

    //endregion

    public double calcularAreaTriangulo(double base, double altura) {
        return base * altura / 2;
    }

    public double calcularAreaCuadrado(double lado) {
        return Math.pow(lado, 2);
    }

    public double calcularAreaCirculo(double radio) {
        return Math.PI * Math.pow(radio, 2);
    }

    public double calcularAreaRectangulo(double base, double altura) {
        return base * altura;
    }
}
