package ejercicio1.paridad;

public class DeterminadorParidad {
    public Paridad determinar(int numero) {
        if (numero % 2 == 0) {
            return Paridad.PAR;
        }

        return Paridad.IMPAR;
    }
}
