package ejercicio1.paridadApp;

import ejercicio1.paridad.DeterminadorParidad;
import ejercicio1.paridad.Paridad;

import java.util.Scanner;

public class ParidadApp {

    public static void main (String[] args) {
        DeterminadorParidad determinadorParidad = new DeterminadorParidad();
        System.out.println("** 1. Determinar Paridad **\n");
        System.out.print("Escribe un número: ");

        try (Scanner scanner = new Scanner(System.in)) {
            int numero = scanner.nextInt();
            Paridad paridad = determinadorParidad.determinar(numero);

            System.out.println("El número " + numero + " es " + paridad);
        }
    }
}
