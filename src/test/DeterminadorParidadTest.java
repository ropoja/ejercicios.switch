package test;

import ejercicio1.paridad.DeterminadorParidad;
import ejercicio1.paridad.Paridad;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeterminadorParidadTest {
    private DeterminadorParidad determinadorParidad;

    @Before
    public void setUp() {
        determinadorParidad = new DeterminadorParidad();
    }

    @Test
    public void determinarParidadPar() {
        Paridad det = determinadorParidad.determinar(2);
        assertEquals(Paridad.PAR, det);
    }

    @Test
    public void determinarParidadImpar() {
        Paridad det = determinadorParidad.determinar(1);
        assertEquals(Paridad.IMPAR, det);
    }

    @Test
    public void determinarParidadCero() {
        Paridad det = determinadorParidad.determinar(0);
        assertEquals(Paridad.PAR, det);
    }

}